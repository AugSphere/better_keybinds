--This could possibly go in keybindings.lua, but I figured may as well keep the organization
-- as moddable as possible.  Paying it forward, I guess.

keyCategories['betterkeymaps'] = "Better Keymaps"
keyCategories['buildmode'] = "Build Mode"

table.insert(keyCategoryOrder, 'betterkeymaps')
table.insert(keyCategoryOrder, 'buildmode')

exclusiveSets = {
	['buildmode'] = "Build Mode"
}
