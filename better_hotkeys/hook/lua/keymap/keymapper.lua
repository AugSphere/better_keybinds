
local exclusiveSets = import('/lua/keymap/keycategories.lua').exclusiveSets

function SetUserKeyMapping(key, oldKey, action)
	LOG("Entered SetUserKeyMapping")
	
	if not key or not action then return end
	
	local keyMapping = GetKeyMappingDetails()
	local keyActions = GetKeyActions()

	if not ((exclusiveSets[keyActions[action].category:lower()] ~= nil or
					 exclusiveSets[keyMapping[key].category:lower()] ~= nil) and
					 (keyActions[action].category:lower() ~= keyMapping[key].category:lower())) then
					 
		LOG("Clearing mapping")
		ClearUserKeyMapping(key)
  	end
	local newUserMap = GetCurrentKeyMap()
	local newDebugMap = GetUserDebugKeyMap()

	if oldKey ~= nil then
		if IsKeyInMap(oldKey, newDebugMap) then
			newDebugMap[oldKey] = nil
		elseif IsKeyInMap(oldKey, newUserMap) then
			newUserMap[oldKey] = nil
		end
	end

	if IsActionInMap(action, newUserMap) or IsActionInMap(action, import('defaultKeyMap.lua').defaultKeyMap) then
		LOG('Keybindings adding key "'..key .. '" in user map for action: ' .. action)
		newUserMap[key] = action
	elseif IsActionInMap(action, newDebugMap) or IsActionInMap(action, import('defaultKeyMap.lua').debugKeyMap) then
		LOG('Keybindings adding key "'..key .. '" in debug map for action: ' .. action)
		newDebugMap[key] = action
	else
		LOG('Keybindings adding key "'..key .. '" in user map for action: ' .. action)
		newUserMap[key] = action
	end

	Prefs.SetToCurrentProfile("UserKeyMap", newUserMap)
	Prefs.SetToCurrentProfile("UserDebugKeyMap", newDebugMap)
end