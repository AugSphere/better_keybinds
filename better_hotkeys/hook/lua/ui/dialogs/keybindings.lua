-- the following section is only needed so long as this is a mod.  If this is ever integrated,
-- simply apply these changes to the applicable lines in the main file.

--redefines the hotkey categories and forces a recalculation

LOG("Reassigning key categories")
local keyCategories = import('/lua/keymap/keycategories.lua').keyCategories
LOG("Reassigning key category order")
local keyCategoryOrder = import('/lua/keymap/keycategories.lua').keyCategoryOrder

local exclusiveSets = import('/lua/keymap/keycategories.lua').exclusiveSets

-- for k, v in pairs( keyGroups ) do
-- 	for name, data in pairs(v) do
		
--	LOG('k: ' .. tostring(k) .. ', v: ', data)
--  end
-- end

local keyGroups = {}
for order, category in keyCategoryOrder do
	local name = string.lower(category)
	keyGroups[name] = {}
	keyGroups[name].order = order
	keyGroups[name].name = name
	keyGroups[name].text = LOC(keyCategories[category])
	keyGroups[name].collapsed = linesCollapsed
	keyGroups[name].visible = 0
	keyGroups[name].bindings = 0
end

FormatData()
--CreateUI()

----

-- copying the following is necessary because of the fact that the nested functions are 
-- actually what's being changed.

local function EditActionKey(parent, action, currentKey)
	LOG("Entered EditActionKey()")
	local dialogContent = Group(parent)
	dialogContent.Height:Set(170)
	dialogContent.Width:Set(400)

	local keyPopup = Popup(popup, dialogContent)

	local cancelButton = UIUtil.CreateButtonWithDropshadow(dialogContent, '/BUTTON/medium/', "<LOC _Cancel>")
	LayoutHelpers.AtBottomIn(cancelButton, dialogContent, 15)
	LayoutHelpers.AtRightIn(cancelButton, dialogContent, -2)
	cancelButton.OnClick = function(self, modifiers)
		keyPopup:Close()
	end

	local okButton = UIUtil.CreateButtonWithDropshadow(dialogContent, '/BUTTON/medium/', "<LOC _Ok>")
	LayoutHelpers.AtBottomIn(okButton, dialogContent, 15)
	LayoutHelpers.AtLeftIn(okButton, dialogContent, -2)

	local helpText = MultiLineText(dialogContent, UIUtil.bodyFont, 16, UIUtil.fontColor)
	LayoutHelpers.AtTopIn(helpText, dialogContent, 10)
	LayoutHelpers.AtHorizontalCenterIn(helpText, dialogContent)
	helpText.Width:Set(dialogContent.Width() - 10)
	helpText:SetText(LOC("<LOC key_binding_0002>Hit the key combination you'd like to assign"))
	helpText:SetCenteredHorizontally(true)

	local keyText = UIUtil.CreateText(dialogContent, FormatKeyName(currentKey), 24)
	keyText:SetColor(UIUtil.factionBackColor)
	LayoutHelpers.Above(keyText, okButton)
	LayoutHelpers.AtHorizontalCenterIn(keyText, dialogContent)

	dialogContent:AcquireKeyboardFocus(false)
	keyPopup.OnClose = function(self)
		dialogContent:AbandonKeyboardFocus()
	end

	local keyCodeLookup = KeyMapper.GetKeyCodeLookup()
	local keyAdder = {}
	local keyPattern

	local function AddKey(keyCode, modifiers)
		local key = keyCodeLookup[keyCode]
		if not key then
			return
		end

		local keyComboName = ""
		keyPattern = ""

		if key ~= 'Ctrl' and modifiers.Ctrl then
			keyPattern = keyPattern .. keyNames['11'] .. "-"
			keyComboName = keyComboName .. LOC(properKeyNames[keyNames['11']]) .. "-"
		end

		if key ~= 'Alt' and modifiers.Alt then
			keyPattern = keyPattern .. keyNames['12'] .. "-"
			keyComboName = keyComboName .. LOC(properKeyNames[keyNames['12']]) .. "-"
		end

		if key ~= 'Shift' and modifiers.Shift then
			keyPattern = keyPattern .. keyNames['10'] .. "-"
			keyComboName = keyComboName .. LOC(properKeyNames[keyNames['10']]) .. "-"
		end

		keyPattern = keyPattern .. key
		keyComboName = keyComboName .. LOC(properKeyNames[key])

		keyText:SetText(keyComboName)
	end

	local oldHandleEvent = dialogContent.HandleEvent
	dialogContent.HandleEvent = function(self, event)
		if event.Type == 'KeyDown' then
			AddKey(event.RawKeyCode, event.Modifiers)
		end

		oldHandleEvent(self, event)
	end

	local function AssignKey()
			LOG("Entered AssignKey()")
		local function ClearShiftKey()
			KeyMapper.ClearUserKeyMapping("Shift-" .. keyPattern)
			LOG("Keybindings clearing Shift-"..keyPattern)
		end

		local function MapKey()
				LOG("Entered MapKey()")
			KeyMapper.SetUserKeyMapping(keyPattern, currentKey, action)

			-- auto-assign shift action, e.g. 'shift_attack' for 'attack' action
			local target = KeyMapper.GetShiftAction(action, 'orders')
			if target and not KeyMapper.ContainsKeyModifiers(keyPattern) then
			   KeyMapper.SetUserKeyMapping('Shift-' .. keyPattern, target.key, target.name)
			end
			
			-- checks if hotbuild modifier keys are conflicting with already mapped actions
			local keyMapping = KeyMapper.GetKeyMappingDetails()
			if keyMapping[keyPattern] and keyMapping[keyPattern].category == "HOTBUILDING" and
				exclusiveSets[keyMapping[keyPattern].category:lower()] == nil then
				local hotKey = "Shift-" .. keyPattern
				if keyMapping[hotKey] then
					UIUtil.QuickDialog(popup, 
						LOCF("<LOC key_binding_0006>The %s key is already mapped under %s category, are you sure you want to clear it for the following action? \n\n %s",
							hotKey, keyMapping[hotKey].category, keyMapping[hotKey].name),
						"<LOC _Yes>", ClearShiftKey,
						"<LOC _No>", nil, nil, nil, true,
						{escapeButton = 2, enterButton = 1, worldCover = false})
				end
			end
			keyTable = FormatData()
			keyContainer:Filter(keyword)
		end
		
		-- LOG(keyMapping[keyPattern].category)
		-- LOG(action.category)
		-- LOG(action)

		-- checks if this key is already assigned to some other action
		local keyMapping = KeyMapper.GetKeyMappingDetails()
		local keyActions = KeyMapper.GetKeyActions()
		
			
		if keyMapping[keyPattern] and keyMapping[keyPattern].id ~= action then
			--and ((keyMapping[keyPattern].category ~= "buildmode") == (action.category ~= "buildmode")) 
			
				local performCheck = true
				LOG("action: " .. (keyActions[action].category:lower() or "nil"))
				LOG("keyPattern: " .. (keyMapping[keyPattern].category:lower() or "nil"))
				LOG("exclusive action: " .. (exclusiveSets[keyActions[action].category:lower()] or "nil"))
				LOG("exclusive keyPattern: " .. (exclusiveSets[keyMapping[keyPattern].category:lower()] or "nil"))
				

				for name, data in pairs(exclusiveSets) do
			   LOG('name: ' .. tostring(name) .. ', data: ', data)
			 end

						
				if (exclusiveSets[keyActions[action].category:lower()] ~= nil or
					 exclusiveSets[keyMapping[keyPattern].category:lower()] ~= nil) and
					 (keyActions[action].category:lower() ~= keyMapping[keyPattern].category:lower()) then
					 
						LOG("Keybind is already set, but as this is a mutually exclusive set, we will set it anyway.")
					 
						MapKey()
  			else
					UIUtil.QuickDialog(popup, 
						LOCF("<LOC key_binding_0006>The %s key is already mapped under %s category, are you sure you want to clear it for the following action? \n\n %s",
							keyPattern, keyMapping[keyPattern].category, keyMapping[keyPattern].name),
						"<LOC _Yes>", MapKey,
						"<LOC _No>", nil, nil, nil, true,
						{escapeButton = 2, enterButton = 1, worldCover = false})
		  		
		  	end
		else
			MapKey()
		end
	end

	okButton.OnClick = function(self, modifiers)
		AssignKey()
		keyPopup:Close()
	end
end


local function AssignCurrentSelection()
	for k, v in keyTable do
		if v.selected then
			EditActionKey(popup, v.action, v.key)
			break
		end
	end
end


-- create a line with dynamically updating UI elements based on type of data line
function CreateLine()
	local keyBindingWidth = 210
	local line = Bitmap(keyContainer)
	line.Left:Set(keyContainer.Left)
	line.Right:Set(keyContainer.Right)
	line.Height:Set(20)

	line.key = UIUtil.CreateText(line, '', 16, "Arial")
	line.key:DisableHitTest()
	line.key:SetAlpha(0.9)

	line.description = UIUtil.CreateText(line, '', 16, "Arial")
	line.description:DisableHitTest()
	line.description:SetClipToWidth(true)
	line.description.Width:Set(line.Right() - line.Left() - keyBindingWidth)
	line.description:SetAlpha(0.9)

	line.Height:Set(function() return line.key.Height() + 4 end)
	line.Width:Set(function() return line.Right() - line.Left() end)

	line.statistics = UIUtil.CreateText(line, '', 16, "Arial")
	line.statistics:EnableHitTest()
	line.statistics:SetColor('FF9A9A9A') --#FF9A9A9A'
	line.statistics:SetAlpha(0.9)

	Tooltip.AddControlTooltip(line.statistics,
	{
		text = 'Category Statistics',
		body = 'Show total of bound actions and total of all actions in this category of keys'
	})

	LayoutHelpers.AtLeftIn(line.description, line, keyBindingWidth)
	LayoutHelpers.AtVerticalCenterIn(line.description, line)
	LayoutHelpers.AtRightIn(line.key, line, line.Width() - keyBindingWidth + 30)
	LayoutHelpers.AtVerticalCenterIn(line.key, line)
	LayoutHelpers.AtRightIn(line.statistics, line, 10)
	LayoutHelpers.AtVerticalCenterIn(line.statistics, line)

	line.HandleEvent = function(self, event)
		if event.Type == 'MouseEnter' then
			line:SetAlpha(0.9)
			line.key:SetAlpha(1.0)
			line.description:SetAlpha(1.0)
			line.statistics:SetAlpha(1.0)
			PlaySound(Sound({Cue = "UI_Menu_Rollover_Sml", Bank = "Interface"}))
		elseif event.Type == 'MouseExit' then
			line:SetAlpha(1.0)
			line.key:SetAlpha(0.9)
			line.description:SetAlpha(0.9)
			line.statistics:SetAlpha(0.9)
		elseif self.data.type == 'entry' then
			if event.Type == 'ButtonPress' then
				SelectLine(self.data.index)
				keyFilter.text:AcquireFocus()
				return true
			elseif event.Type == 'ButtonDClick' then
				SelectLine(self.data.index)
				AssignCurrentSelection()
				return true
			end
		elseif self.data.type == 'header' and (event.Type == 'ButtonPress' or event.Type == 'ButtonDClick') then
			if string.len(keyword) == 0 then
				ToggleLines(self.data.category)
				keyFilter.text:AcquireFocus()

				if keyGroups[self.data.category].collapsed then
				   self.toggle.txt:SetText('+')
				else
				   self.toggle.txt:SetText('-')
				end
				PlaySound(Sound({Cue = "UI_Menu_MouseDown_Sml", Bank = "Interface"}))
				return true
			end
		end
		return false
	end

	line.AssignKeyBinding = function(self)
		SelectLine(self.data.index)
		AssignCurrentSelection()
	end

	line.UnbindKeyBinding = function(self)
		if keyTable[self.data.index].key then
			SelectLine(self.data.index)
			UnbindCurrentSelection()
		end
	end

	line.toggle = CreateToggle(line,
		 'FF1B1A1A',  --#FF1B1A1A'
		 UIUtil.factionTextColor,
		 line.key.Height() + 4, 18, '+')
	LayoutHelpers.AtLeftIn(line.toggle, line, keyBindingWidth - 30)
	LayoutHelpers.AtVerticalCenterIn(line.toggle, line)
	Tooltip.AddControlTooltip(line.toggle,
	{
		text = 'Toggle Category',
		body = 'Toggle visibility of all actions for this category of keys'
	})

	line.assignKeyButton = CreateToggle(line,
		 '645F5E5E',  --#735F5E5E'
		 'FFAEACAC',  --#FFAEACAC'
		 line.key.Height() + 4, 18, '+')
	LayoutHelpers.AtLeftIn(line.assignKeyButton, line)
	LayoutHelpers.AtVerticalCenterIn(line.assignKeyButton, line)
	Tooltip.AddControlTooltip(line.assignKeyButton,
	{
		text = LOC("<LOC key_binding_0003>Assign Key"),
		body = 'Opens a dialog that allows assigning key binding for a given action'
	})
	line.assignKeyButton.OnMouseClick = function(self)
		line:AssignKeyBinding()
		return true
	end

	line.unbindKeyButton = CreateToggle(line,
		 '645F5E5E',  --#645F5E5E'
		 'FFAEACAC',  --#FFAEACAC'
		 line.key.Height() + 4, 18, 'x')
	LayoutHelpers.AtRightIn(line.unbindKeyButton, line)
	LayoutHelpers.AtVerticalCenterIn(line.unbindKeyButton, line)
	Tooltip.AddControlTooltip(line.unbindKeyButton,
	{
		text = LOC("<LOC key_binding_0007>Unbind Key"),
		body = 'Removes currently assigned key binding for a given action'
	})

	line.unbindKeyButton.OnMouseClick = function(self)
		line:UnbindKeyBinding()
		return true
	end

	line.Update = function(self, data, lineID)
		line:SetSolidColor(GetLineColor(lineID, data))
		line.data = table.copy(data)

		if data.type == 'header' then
			if keyGroups[self.data.category].collapsed then
			   self.toggle.txt:SetText('+')
			else
			   self.toggle.txt:SetText('-')
			end
			local stats = keyGroups[data.category].bindings .. ' / ' ..
						  keyGroups[data.category].visible  ..' Actions Bound'
			line.toggle:Show()
			line.assignKeyButton:Hide()
			line.unbindKeyButton:Hide()
			line.description:SetText(data.text)
			line.description:SetFont(UIUtil.titleFont, 16)
			line.description:SetColor(UIUtil.factionTextColor)
			line.key:SetText('')
			line.statistics:SetText(stats)
		elseif data.type == 'spacer' then
			line.toggle:Hide()
			line.assignKeyButton:Hide()
			line.unbindKeyButton:Hide()
			line.key:SetText('')
			line.description:SetText('')
			line.statistics:SetText('')
		elseif data.type == 'entry' then
			line.toggle:Hide()
			line.key:SetText(data.keyText)
			line.key:SetColor('ffffffff') --#ffffffff'
			line.key:SetFont('Arial', 16)
			line.description:SetText(data.text)
			line.description:SetFont('Arial', 16)
			line.description:SetColor(UIUtil.fontColor)
			line.statistics:SetText('')
			line.unbindKeyButton:Show()
			line.assignKeyButton:Show()
		end
	end
	return line
end



function CloseUI()
	LOG('Keybindings CloseUI')
	if popup then
	   popup:Close()
	   popup = false
	end
end
function CreateUI()
	LOG('Keybindings CreateUI')
	if WorldIsLoading() or (import('/lua/ui/game/gamemain.lua').supressExitDialog == true) then
		return
	end

	if popup then
		CloseUI()
		return
	end
	keyword = ''
	keyTable = FormatData()

	local dialogContent = Group(GetFrame(0))
	dialogContent.Width:Set(980)
	dialogContent.Height:Set(730)

	popup = Popup(GetFrame(0), dialogContent)
	popup.OnShadowClicked = CloseUI
	popup.OnEscapePressed = CloseUI
	popup.OnDestroy = function(self)
		RemoveInputCapture(dialogContent)
	end

	local title = UIUtil.CreateText(dialogContent, LOC("<LOC key_binding_0000>Key Bindings"), 22)
	LayoutHelpers.AtTopIn(title, dialogContent, 12)
	LayoutHelpers.AtHorizontalCenterIn(title, dialogContent)

	local offset = dialogContent.Width() / 5

	local closeButton = UIUtil.CreateButtonWithDropshadow(dialogContent, "/BUTTON/medium/", LOC("<LOC _Close>"))
	closeButton.Width:Set(200)
	LayoutHelpers.AtBottomIn(closeButton, dialogContent, 10)
	LayoutHelpers.AtRightIn(closeButton, dialogContent, offset - (closeButton.Width() / 2))
	Tooltip.AddControlTooltip(closeButton,
	{
		text = 'Close Dialog', body = 'Closes this dialog and confirms assignments of key bindings'
	})
	closeButton.OnClick = function(self, modifiers)
		-- confirmation of changes will occur on OnClosed of this UI
		CloseUI() 
	end

	popup.OnClosed = function(self)
		ConfirmNewKeyMap()
	end

	local defaultButton = UIUtil.CreateButtonWithDropshadow(dialogContent, "/BUTTON/medium/", LOC("<LOC key_binding_0004>Default Preset"))
	defaultButton.Width:Set(200)
	LayoutHelpers.AtBottomIn(defaultButton, dialogContent, 10)
	LayoutHelpers.AtLeftIn(defaultButton, dialogContent, offset - (defaultButton.Width() / 2))
	defaultButton.OnClick = function(self, modifiers)
		UIUtil.QuickDialog(popup, "<LOC key_binding_0005>Are you sure you want to reset all key bindings to the default (GPG) preset?",
			"<LOC _Yes>", ResetBindingToDefaultKeyMap,
			"<LOC _No>", nil, nil, nil, true,
			{escapeButton = 2, enterButton = 1, worldCover = false})
	end
	Tooltip.AddControlTooltip(defaultButton,
	{
		text = LOC("<LOC key_binding_0004>Default Preset"),
		body = 'Reset all key bindings to the default (GPG) preset'
	})

	local hotbuildButton = UIUtil.CreateButtonWithDropshadow(dialogContent, "/BUTTON/medium/", LOC("<LOC key_binding_0009>Hotbuild Preset"))
	hotbuildButton.Width:Set(200)
	LayoutHelpers.AtBottomIn(hotbuildButton, dialogContent, 10)
	LayoutHelpers.AtHorizontalCenterIn(hotbuildButton, dialogContent)
	hotbuildButton.OnClick = function(self, modifiers)
		UIUtil.QuickDialog(popup, "<LOC key_binding_0008>Are you sure you want to reset all key bindings to the hotbuild (FAF) preset?",
			"<LOC _Yes>", ResetBindingToHotbuildKeyMap,
			"<LOC _No>", nil, nil, nil, true,
			{escapeButton = 2, enterButton = 1, worldCover = false})
	end
	Tooltip.AddControlTooltip(hotbuildButton,
	{
		text = LOC("<LOC key_binding_0009>Hotbuild Preset"),
		body = 'Reset all key bindings to the hotbuild (FAF) preset'
	})

	dialogContent.HandleEvent = function(self, event)
		if event.Type == 'KeyDown' then
			if event.KeyCode == UIUtil.VK_ESCAPE or event.KeyCode == UIUtil.VK_ENTER or event.KeyCode == 342 then
				closeButton:OnClick()
			end
		end
	end

	keyFilter = Bitmap(dialogContent)
	keyFilter:SetSolidColor('FF282828')-- #FF282828
	keyFilter.Left:Set(function() return dialogContent.Left() + 63 end)
	keyFilter.Right:Set(function() return dialogContent.Right() - 6 end)
	keyFilter.Top:Set(function() return title.Bottom() + 10 end)
	keyFilter.Bottom:Set(function() return title.Bottom() + 40 end)
	keyFilter.Width:Set(function() return keyFilter.Right() - keyFilter.Left() end)
	keyFilter.Height:Set(function() return keyFilter.Bottom() - keyFilter.Top() end)

	keyFilter:EnableHitTest()
	import('/lua/ui/game/tooltip.lua').AddControlTooltip(keyFilter,
	{
		text = 'Key Binding Filter',
		body = 'Filter all actions by typing either: ' ..
		'\n - full key binding "CTRL+K" ' ..
		'\n - partial key binding "CTRL" ' ..
		'\n - full action name "Self-Destruct" ' ..
		'\n - partial action name "Self" '..
		'\n\n Note that collapsing of key categories is disabled while this filter contains some text'
	}, nil, 200)

	keyFilter.label = UIUtil.CreateText(dialogContent, 'Filter', 17)
	keyFilter.label:SetColor('FF929191') -- #FF929191
	keyFilter.label:SetFont(UIUtil.titleFont, 17)
	LayoutHelpers.AtVerticalCenterIn(keyFilter.label, keyFilter, 2)
	LayoutHelpers.AtLeftIn(keyFilter.label, dialogContent, 9)

	local text = LOC("<LOC key_binding_filterInfo>Type key binding or name of action")
	keyFilter.info = UIUtil.CreateText(keyFilter, text, 17, UIUtil.titleFont)
	keyFilter.info:SetColor('FF727171') -- #FF727171
	keyFilter.info:DisableHitTest()
	LayoutHelpers.AtHorizontalCenterIn(keyFilter.info, keyFilter, -7)
	LayoutHelpers.AtVerticalCenterIn(keyFilter.info, keyFilter, 2)

	keyFilter.text = Edit(keyFilter)
	keyFilter.text:SetForegroundColor('FFF1ECEC') -- #FFF1ECEC
	keyFilter.text:SetBackgroundColor('04E1B44A') -- #04E1B44A
	keyFilter.text:SetHighlightForegroundColor(UIUtil.highlightColor)
	keyFilter.text:SetHighlightBackgroundColor("880085EF") --#880085EF
	keyFilter.text.Height:Set(function() return keyFilter.Bottom() - keyFilter.Top() - 10 end)
	keyFilter.text.Left:Set(function() return keyFilter.Left() + 5 end)
	keyFilter.text.Right:Set(function() return keyFilter.Right() end)
	LayoutHelpers.AtVerticalCenterIn(keyFilter.text, keyFilter)
	keyFilter.text:AcquireFocus()
	keyFilter.text:SetText('')
	keyFilter.text:SetFont(UIUtil.titleFont, 17)
	keyFilter.text:SetMaxChars(20)
	keyFilter.text.OnTextChanged = function(self, newText, oldText)
		-- interpret plus chars as spaces for easier key filtering
		keyword = string.gsub(string.lower(newText), '+', ' ')
		keyword = string.gsub(string.lower(keyword), '  ', ' ')
		keyword = string.gsub(string.lower(keyword), '  ', ' ')
		if string.len(keyword) == 0 then
			for k, v in keyGroups do
				v.collapsed = true
			end
			for k, v in keyTable do
				v.collapsed = true
			end
		end
		keyContainer:Filter(keyword)
		keyContainer:ScrollSetTop(nil, 0)
	end

	keyFilter.clear = UIUtil.CreateText(keyFilter.text, 'X', 17, "Arial Bold")
	keyFilter.clear:SetColor('FF8A8A8A') -- #FF8A8A8A
	keyFilter.clear:EnableHitTest()
	LayoutHelpers.AtVerticalCenterIn(keyFilter.clear, keyFilter.text, 1)
	LayoutHelpers.AtRightIn(keyFilter.clear, keyFilter.text, 9)

	keyFilter.clear.HandleEvent = function(self, event)
		if event.Type == 'MouseEnter' then
			keyFilter.clear:SetColor('FFC9C7C7') -- #FFC9C7C7
		elseif event.Type == 'MouseExit' then
			keyFilter.clear:SetColor('FF8A8A8A') -- #FF8A8A8A
		elseif event.Type == 'ButtonPress' or event.Type == 'ButtonDClick' then
			keyFilter.text:SetText('')
			keyFilter.text:AcquireFocus()
		end
		return true
	end
	Tooltip.AddControlTooltip(keyFilter.clear,
	{
		text = 'Clear Filter',
		body = 'Clears text that was typed in the filter field.'
	})

	keyContainer = Group(dialogContent)
	keyContainer.Left:Set(function() return dialogContent.Left() + 10 end)
	keyContainer.Right:Set(function() return dialogContent.Right() - 20 end)
	keyContainer.Top:Set(function() return keyFilter.Bottom() + 10 end)
	keyContainer.Bottom:Set(function() return defaultButton.Top() - 10 end)
	keyContainer.Height:Set(function() return keyContainer.Bottom() - keyContainer.Top() - 10 end)
	keyContainer.top = 0
	UIUtil.CreateLobbyVertScrollbar(keyContainer)

	local index = 1
	keyEntries = {}
	keyEntries[index] = CreateLine()
	LayoutHelpers.AtTopIn(keyEntries[1], keyContainer)

	index = index + 1
	while keyEntries[table.getsize(keyEntries)].Top() + (2 * keyEntries[1].Height()) < keyContainer.Bottom() do
		keyEntries[index] = CreateLine()
		LayoutHelpers.Below(keyEntries[index], keyEntries[index-1])
		index = index + 1
	end

	local height = keyContainer.Height()
	local items = math.floor(keyContainer.Height() / keyEntries[1].Height())

	local GetLinesTotal = function()
		return table.getsize(keyEntries)
	end

	local function GetLinesVisible()
		return table.getsize(linesVisible)
	end

	-- Called when the scrollbar for the control requires data to size itself
	-- GetScrollValues must return 4 values in this order:
	-- rangeMin, rangeMax, visibleMin, visibleMax
	-- axis can be "Vert" or "Horz"
	keyContainer.GetScrollValues = function(self, axis)
		local size = GetLinesVisible()
		local visibleMax = math.min(self.top + GetLinesTotal(), size)
		return 0, size, self.top, visibleMax
	end

	-- Called when the scrollbar wants to scroll a specific number of lines (negative indicates scroll up)
	keyContainer.ScrollLines = function(self, axis, delta)
		self:ScrollSetTop(axis, self.top + math.floor(delta))
	end

	-- Called when the scrollbar wants to scroll a specific number of pages (negative indicates scroll up)
	keyContainer.ScrollPages = function(self, axis, delta)
		self:ScrollSetTop(axis, self.top + math.floor(delta) * GetLinesTotal())
	end

	-- Called when the scrollbar wants to set a new visible top line
	keyContainer.ScrollSetTop = function(self, axis, top)
		top = math.floor(top)
		if top == self.top then return end
		local size = GetLinesVisible()
		self.top = math.max(math.min(size - GetLinesTotal() , top), 0)
		self:CalcVisible()
	end

	-- Called to determine if the control is scrollable on a particular access. Must return true or false.
	keyContainer.IsScrollable = function(self, axis)
		return true
	end

	-- Determines what control lines should be visible or not
	keyContainer.CalcVisible = function(self)
		for i, line in keyEntries do
			local id = i + self.top
			local index = linesVisible[id]
			local data = keyTable[index]

			if data then
				line:Update(data, id)
			else
				line:SetSolidColor('00000000') --#00000000
				line.key:SetText('')
				line.description:SetText('')
				line.statistics:SetText('')
				line.toggle:Hide()
				line.assignKeyButton:Hide()
				line.unbindKeyButton:Hide()
			end
		end
		keyFilter.text:AcquireFocus()
	end

	keyContainer.HandleEvent = function(control, event)
		if event.Type == 'WheelRotation' then
			local lines = 1
			if event.WheelRotation > 0 then
				lines = -1
			end
			control:ScrollLines(nil, lines)
		end
	end
	-- filter all key-bindings by checking if either text, action, or a key contains target string
	keyContainer.Filter = function(self, target)
		local headersVisible = {}
		linesVisible = {}

		if not target or string.len(target) == 0 then
			keyFilter.info:Show()
			for k, v in keyTable do
				if v.type == 'header' then
					table.insert(linesVisible, k)
					keyGroups[v.category].visible = v.count
					keyGroups[v.category].bindings = 0
				elseif v.type == 'entry' then
					if not v.collapsed then
						table.insert(linesVisible, k)
					end
					if v.key then
						keyGroups[v.category].bindings = keyGroups[v.category].bindings + 1
					end
				end
			end
		else
			keyFilter.info:Hide()
			for k, v in keyTable do
				local match = false
				if v.type == 'header' then
					keyGroups[v.category].visible = 0
					keyGroups[v.category].bindings = 0
					if not headersVisible[k] then
						headersVisible[k] = true
						table.insert(linesVisible, k)
						keyGroups[v.category].collapsed = true
					end
				elseif v.type == 'entry' and v.filters then
					if string.find(v.filters.text, target) then
						match = true
						v.filterMatch = 'text'
					elseif string.find(v.filters.key, target) then
						match = true
						v.filterMatch = 'key'
					elseif string.find(v.filters.action, target) then
						match = true
						v.filterMatch = 'action'
					elseif string.find(v.filters.category, target) then
						match = true
						v.filterMatch = 'category'
					else
						match = false
						v.filterMatch = nil
					end
					if match then
						if not headersVisible[v.header] then
							headersVisible[v.header] = true
							table.insert(linesVisible, v.header)
						end
						keyGroups[v.category].collapsed = false
						keyGroups[v.category].visible = keyGroups[v.category].visible + 1
						table.insert(linesVisible, k)
						if v.key then
							keyGroups[v.category].bindings = keyGroups[v.category].bindings + 1
						end
					end
				end
			end
		end
		self:CalcVisible()
	end
	keyFilter.text:SetText('')
end
